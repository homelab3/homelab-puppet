# Homelab-Puppet

Puppet repository for my homelab.

## Table of Content

<!-- vim-markdown-toc GitLab -->

* [Introduction and Goals](#introduction-and-goals)
* [Installation](#installation)
    * [Puppet server](#puppet-server)
    * [Puppet client](#puppet-client)
* [Configuration](#configuration)
    * [Puppet server](#puppet-server-1)
    * [r10k](#r10k)
    * [Puppet client](#puppet-client-1)
* [Architecture](#architecture)
* [Branches](#branches)
* [Puppet Commands](#puppet-commands)

<!-- vim-markdown-toc -->

## Introduction and Goals

The goal of this repository is to configure and maintain VMs using Puppet as Infrastructure-as-Code.

## Installation

### Puppet server

On Ubuntu Server 22.04:

```bash
# Puppet server
wget https://apt.puppet.com/puppet8-release-jammy.deb
sudo dpkg -i puppet8-release-jammy.deb
sudo apt update
sudo apt install puppetserver
sudo systemctl enable puppetserver
sudo systemctl start puppetserver

# R10K
sudo apt install ruby-rubygems
sudo gem install r10k
```

### Puppet client

Puppet client is already installed from the Terraform image.

## Configuration

### Puppet server

Update Puppet configuration under /etc/puppetlabs/puppet/puppet.conf:

```
[main]
    dns_alt_names = SERVER_FQDN

[agent]
    server = SERVER_FQDN

[server]
vardir = /opt/puppetlabs/server/data/puppetserver
logdir = /var/log/puppetlabs/puppetserver
rundir = /var/run/puppetlabs/puppetserver
pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid
codedir = /etc/puppetlabs/code
```

### r10k

Create /etc/puppetlabs/r10k/r10k.yaml with:

```
# The location to use for storing cached Git repos
:cachedir: '/var/cache/r10k'

# A list of git repositories to create
:sources:
# This will clone the git repository and instantiate an environment per
# branch in /etc/puppetlabs/code/environments
  :homelab:
    remote: 'https://gitlab.com/homelab3/homelab-puppet.git'
    basedir: '/etc/puppetlabs/code/environments'
```

Download code:

```
sudo r10k deploy --config /etc/puppetlabs/r10k/r10k.yaml environment -pv
```

### Puppet client 

Update /etc/puppet/puppet.conf:

```
[main]
    dns_alt_names = CLIENT_FQDN

[agent]
    server = SERVER_FQDN

[server]
vardir = /opt/puppetlabs/server/data/puppetserver
logdir = /var/log/puppetlabs/puppetserver
rundir = /var/run/puppetlabs/puppetserver
pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid
codedir = /etc/puppetlabs/code

```

## Architecture

The architecture is based on the profile-role architecture. A node includes one or multiple roles. Each role contains one or multiple profiles. Each profile contains one or multiple ressource which will be applied to a node.

```bash
homelab-puppet
├──manifests
│  └──site.pp               <- Starting point with a triage of the node and including roles
├──site                     <- Folder containing the different profiles and roles
│  ├──profile               <- Profiles folder
│  │  └──manifests
│  │     ├──profile1.pp     <- Profile definition
│  │     └──...
│  └──role                  <- Role folder
│     └──manifests
│        ├──role1.pp        <- Role definition
│        └──...
├──environment.conf         <- Environment definition
├──hiera.yaml               <- Facts for Facter
├──Puppetfile               <- List of modules to load
├──README.md
└──LICENSE

```

## Branches

There is one branch for each project environment (common, test and prod) for the Homelab. Each branch only affects its corresponding environment. Futhermore a master and a computer branch exist. The master branch only contains this README while the computer branch contains the configuration for laptop and desktop computers.

## Puppet Commands

```bash
sudo puppet agent -t
```

```bash
sudo r10k deploy --config /etc/r10k.yaml environment -pv
```
